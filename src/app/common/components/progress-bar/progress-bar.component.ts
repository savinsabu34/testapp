import { Component, OnInit, Input } from '@angular/core';
// import { ApexService } from 'src/app/services/apex.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {
  ngOnInit() {}

  @Input()
  type: string = "page";
  @Input()
  loaded:any;
  showLoading: any = true;
  constructor(private router: Router,
    private route: ActivatedRoute) {
    setTimeout(() => {
        this.loaded = "true";
    }, 1000);
  }
}
