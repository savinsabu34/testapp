/** Storage  */
export class Storage {
    static pid: string = "x";
  
    static setJWT(val: string) {
      if (val) {
        sessionStorage.setItem(this.pid + "-jwt", val);
      }
    }
    static getJWT() {
      return sessionStorage.getItem(this.pid + "-jwt");
    }
  }
  