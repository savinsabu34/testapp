import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';


const routes: Routes = [
{ path:"", pathMatch: "full", redirectTo: "auth/signin" },
{ path:"auth", 
  loadChildren: () => import("./modules/auth/auth.module").then(m => m.AuthModule) 
},
{ path:"apex", 
  loadChildren: () => import("./modules/apex/apex.module").then(m => m.ApexModule) 
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
    // preloadingStrategy: PreloadAllModules,
    // enableTracing: true 
  }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
