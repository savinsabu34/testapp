import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInComponent } from './sign-in/sign-in.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { Routes, RouterModule } from '@angular/router';
import { MatSnackBarModule, MatCardModule, MatLabel, MatIconModule, MatHint, MatFormFieldModule, MatButtonModule, MatMenuModule, MatToolbarModule, MatGridListModule, MatSidenavModule, MatSortModule, MatTableModule, MatInputModule, MatSelectModule, MatSliderModule, MatRadioModule, MatListModule, MatProgressSpinnerModule, MatProgressBarModule, MatChipsModule, MatTooltipModule, MatExpansionModule, MatDialogModule, MatAutocompleteModule, MatTabsModule, MatSlideToggleModule, MatPaginatorModule, MatDatepickerModule, MatNativeDateModule, MatStepperModule, MatButtonToggleModule, MatCheckboxModule } from '@angular/material';
import { ProgressBarComponent } from 'src/app/common/components/progress-bar/progress-bar.component';
import { MaterialModule } from 'src/app/common/utils/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const routes: Routes = [
  { path: "signin", component: SignInComponent },
  { path: "signup", component: SignUpComponent },
  { path: "forgotPassword", component: ForgotPasswordComponent },
  { path: "resetPassword", component: ResetPasswordComponent }
];
@NgModule({
  declarations: [SignInComponent, ForgotPasswordComponent, ResetPasswordComponent, SignUpComponent, ProgressBarComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FlexLayoutModule,
    FormsModule,                              
    ReactiveFormsModule,
    // material : !@#$%^&*
    MatCardModule,
    MatFormFieldModule,
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCheckboxModule,
    MatGridListModule,
    MatSidenavModule,
    MatSortModule,
    MatTableModule,
    MatSelectModule,
    MatSliderModule,
    MatRadioModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatChipsModule,
    MatTooltipModule,
    MatExpansionModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    DragDropModule,
    MatStepperModule,
    MatButtonToggleModule,
    MatInputModule
    ],
  exports: [
    ProgressBarComponent,
  ]
})
export class AuthModule { }
