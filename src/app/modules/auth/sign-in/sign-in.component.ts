import { Component, OnInit, ViewChild, Input, ContentChild, AfterViewInit } from '@angular/core';
import { User } from 'src/app/entities/User';
import { Props } from 'src/app/entities/props'
import { FormControl, NgForm, FormGroup } from '@angular/forms';
import { ValidationService } from 'src/app/services/validation.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit,AfterViewInit {

  // user:User=new
  user:User = new User();
  Props=Props;
  @Input()
  control: FormControl;
  errMessage: string;
  isAuthFail:boolean = false;
  myForm1:any;
  userNameErrMsg:string = "Invalid Username!"
  @ViewChild('myForm')
  private myForm: FormGroup;

  constructor(private auth: AuthService, public router: Router) { }

  ngAfterViewInit() {
    console.log(this.myForm.errors);
    this.myForm1 = this.myForm;
    console.log(this.myForm.controls);
    this.checkError();
  }  

  ngOnInit() {
    this.auth.login().subscribe((data:any)=>{
      if(data){
        console.log(data);
        localStorage.clear();
        // 
        localStorage.setItem("allCredentials", JSON.stringify(data));
        // console.log(localStorage.getItem("allCredentials"));
      }
    })
  }

  get errorMessage() {
    if(this.control != undefined){
      for (let propertyName in this.control.errors) {
        if (
          this.control.errors.hasOwnProperty(propertyName) &&
          this.control.touched
        ) {
          return ValidationService.getValidatorErrorMessage(
            propertyName,
            this.control.errors[propertyName],
            this.errMessage
          );
        }
        console.log(this.control.errors);
      }
    }else{
      // console.log(this.control.errors);
      return null;
    }
  }

  checkError(){
    console.log(this.myForm.controls);
  }
  
  
  login(user){
    this.auth.login().subscribe((data:any) => {
      if(data){
        data.forEach(element => {
          if(user.username == element.username && user.password == element.password){
            console.log("login credentials are verified..");
            this.isAuthFail = false;
            this.router.navigate(['apex/home'])
          }else{
            console.log("password or username is wrong..");
            this.isAuthFail = true;
          }
        });
      }
      return false;
    })

  }
}
