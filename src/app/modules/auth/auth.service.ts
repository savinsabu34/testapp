import { Injectable } from '@angular/core';
import { Storage } from '../../common/utils/storage'
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/entities/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  constructor(private http: HttpClient) { }

  public isAuthenticated(): boolean {
    const token = Storage.getJWT();
    return token && token != null;
  }

  private _userUrl = "assets/db/user.json"
  
  login(){
    // this url can be https://randomuser.me/api/0.8/?results=20...
    return this.http.get<User[]>(this._userUrl)
    // return this.http.get<any>(this._userUrl)
  }
  saveUser(data:any){
    return this.http.post<User[]>(this._userUrl, { data: data });
  }
}
