import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page/home-page.component';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSnackBarModule, MatCardModule, MatLabel, MatIconModule, MatHint, MatFormFieldModule, MatButtonModule, MatMenuModule, MatToolbarModule, MatGridListModule, MatSidenavModule, MatSortModule, MatTableModule, MatInputModule, MatSelectModule, MatSliderModule, MatRadioModule, MatListModule, MatProgressSpinnerModule, MatProgressBarModule, MatChipsModule, MatTooltipModule, MatExpansionModule, MatDialogModule, MatAutocompleteModule, MatTabsModule, MatSlideToggleModule, MatPaginatorModule, MatDatepickerModule, MatNativeDateModule, MatStepperModule, MatButtonToggleModule, MatCheckboxModule } from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const routes: Routes = [
  { path: "home", component: HomePageComponent },
];
@NgModule({
  declarations: [HomePageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FlexLayoutModule,
    FormsModule,                              
    ReactiveFormsModule,
    // material : !@#$%^&*
    MatCardModule,
    MatFormFieldModule,
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCheckboxModule,
    MatGridListModule,
    MatSidenavModule,
    MatSortModule,
    MatTableModule,
    MatSelectModule,
    MatSliderModule,
    MatRadioModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatChipsModule,
    MatTooltipModule,
    MatExpansionModule,
    MatDialogModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    DragDropModule,
    MatStepperModule,
    MatButtonToggleModule,
    MatInputModule
  ]
})
export class ApexModule { }
