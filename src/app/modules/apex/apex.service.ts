import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApexService {

  private _userUrl = "assets/db/user.json"
  constructor(private http: HttpClient) { }

  saveUser(data:any){
    return this.http.put(this._userUrl, data);
   }
}
