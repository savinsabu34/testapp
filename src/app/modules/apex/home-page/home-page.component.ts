import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { MatTableDataSource, MatSort } from '@angular/material';
import { User } from 'src/app/entities/User';
import { Props } from 'src/app/entities/props';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  
  jsonObj:any;
  dataSource: MatTableDataSource<User>; 
  displayedColumns = ['id','title','first_name','last_name','email','username','password','dob','phone','role'];
  
  Props=Props;
  isAddUserEnable = false;
  userList:boolean = true;
  user: User = new User();
  titles = ["Mr", "Mrs"];
  userDataSource: User[] =[];
  userFilter: User[];
  isEnableDelete:boolean = false;
  minDate = new Date(2000, 0, 1);
  maxDate = new Date(2020, 0, 1);
  constructor(private auth: AuthService) { }
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.jsonObj = JSON.parse(localStorage.getItem("allCredentials"));
    console.log(this.jsonObj);
    this.dataSource = new MatTableDataSource(this.jsonObj);
    console.log(this.dataSource);
    this.dataSource.sort = this.sort;
  }

  addExtraUser(){
    this.isAddUserEnable = !this.isAddUserEnable;
    if(this.isAddUserEnable){
      this.user = new User();
      console.log(this.user);
      // debugger;
    }
  }
  saveUser(user:any){
    // this.auth.saveUser(user).subscribe((data:any) => {
    //   if(data){
    //     console.log(data);
    //     console.log("User data stored successfully...");
    //   }
    // })

    //  I don't know how to update json file via angular. I can update json object. but not json file.
    this.jsonObj.push(user);
    this.dataSource = new MatTableDataSource(this.jsonObj);
    this.user = new User();
    this.isAddUserEnable = ! this.isAddUserEnable;
  }
  applyFilter(filterValue){
    this.dataSource.filterPredicate = (data: any, filter) => {
      const dataStr =JSON.stringify(data).toLowerCase();
      return dataStr.indexOf(filter) != -1; 
    }
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
