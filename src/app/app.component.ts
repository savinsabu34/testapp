import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatIconRegistry } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  @Input()
  loaded:any;
  title = 'testApplication';
  menuList: any[] = [];
  sessionUser: any = null;
  constructor(private _iconRegistry: MatIconRegistry, private router: Router,
              private route: ActivatedRoute) {
      
        setTimeout(() => {
          this.router.routerState.root.params.subscribe(params => {
            this.loaded = "true";
          })
        }, 1000);
        
      
    }

}
