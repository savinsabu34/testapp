import { Name } from './Name';

export class User {
    role:string;
    id:string;
    gender:string;
    name:Name = new Name();
    email:string;
    username: string;
    password: string;
    dob:string;
    phone:string;
  }